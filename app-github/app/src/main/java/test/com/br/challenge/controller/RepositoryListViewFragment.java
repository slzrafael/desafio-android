package test.com.br.challenge.controller;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import test.com.br.challenge.BuildConfig;
import test.com.br.challenge.R;
import test.com.br.challenge.adapter.RepositoryAdapter;
import test.com.br.challenge.commom.ProgressFeedbackManager;
import test.com.br.challenge.model.Repository;

/**
 * Created by rafaelfreitas on 11/2/16.
 */
public class RepositoryListViewFragment extends DefaultFragment implements RepositoryListView, RepositoryListViewPresenter.OnRequestFinished {

    @BindView(R.id.list_repositories)
    protected RecyclerView recycler;

    @BindView(R.id.pull_refresh)
    protected SwipeRefreshLayout swype;

    @BindView(R.id.toolbar)
    protected Toolbar toolbar;

    @BindView(R.id.fab_up)
    protected FloatingActionButton fab;

    @Inject
    protected ProgressFeedbackManager feedback;

    @Inject
    protected RepositoryListViewInteractor interactor;

    @Inject
    protected RepositoryListViewPresenter presenter;

    @Inject
    protected Picasso picasso;

    private RepositoryAdapter adapter;

    private int page = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View inflated = inflater.inflate(R.layout.repository_list_fragment, container, false);

        ButterKnife.setDebug(BuildConfig.DEBUG);
        ButterKnife.bind(this, inflated);

        getAppComponent().inject(this);
        getViewComponent().inject(this);

        LinearLayoutManager layout = new LinearLayoutManager(getActivity());
        DividerItemDecoration divider = new DividerItemDecoration(getActivity(), layout.getOrientation());

        recycler.setLayoutManager(layout);
        recycler.setHasFixedSize(true);
        recycler.addItemDecoration(divider);

        toolbar.setTitle("Github JavaPop");
        toolbar.setTitleTextColor(getContext().getColor(android.R.color.white));

        return inflated;
    }

    public RepositoryListViewComponent getViewComponent(){
        return DaggerRepositoryListViewComponent.builder()
                .applicationComponent(getAppComponent())
                .repositoryListViewModule(new RepositoryListViewModule(getActivity()))
                .build();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        swype.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                interactor.onPull(++page, RepositoryListViewFragment.this);
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                interactor.onClickToBackToTop(recycler);
            }
        });

        recycler.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                interactor.onScrollRecyclerView(fab, recyclerView);
            }

        });

        showProgressBar();
        presenter.getList(++page, this);
    }

    @Override
    public void onSuccess(final List<Repository> repositories) {

        if(adapter == null){
            adapter = new RepositoryAdapter(getActivity(), repositories, interactor, picasso);
            recycler.setAdapter(adapter);

            hideProgressBar();
        } else {
            swype.setRefreshing(false);

            final int lastPositionItem = adapter.getItemCount();

            Snackbar.make(getView(), "Foram carregados novos itens.", Snackbar.LENGTH_LONG).setAction("VER", new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    interactor.onClickToGoForNewItens(recycler, lastPositionItem);
                }

            }).show();

            adapter.getRepositories().addAll(repositories);
            adapter.notifyDataSetChanged();
        }

    }
    @Override
    public void showProgressBar() {
        feedback.show();
    }

    @Override
    public void hideProgressBar() {
        feedback.dismiss();
    }

    @Override
    public void onError(RuntimeException e) {
        hideProgressBar();

        new AlertDialog.Builder(getContext())
                .setTitle("Atencão")
                .setMessage("Ocoreu um erro")
                .setNeutralButton("OK", null).show();
    }

}
