package test.com.br.challenge.controller;

import android.app.Activity;

import com.google.gson.Gson;

import test.com.br.challenge.app.ActivitiyScope;
import test.com.br.challenge.commom.ProgressFeedbackManager;
import test.com.br.challenge.commom.ViewProgressFeedbackManagerImpl;
import test.com.br.challenge.net.GithubAPI;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;

/**
 * Created by rafael-iteris on 11/7/16.
 */

@Module
public class PullListModule {

    private Activity activity;

    public PullListModule(Activity activity) {
        this.activity = activity;
    }

    @Provides @ActivitiyScope
    public ProgressFeedbackManager provideProgress(){
        return new ViewProgressFeedbackManagerImpl(activity);
    }

    @Provides @ActivitiyScope
    public PullListViewInteractor provideInteractor(PullListViewPresenter presenter){
        return new PullListViewInteractorImpl(activity, presenter);
    }

    @Provides @ActivitiyScope
    public PullListViewPresenter providePresenter(GithubAPI api, OkHttpClient client, Gson gson){
        return new PullListViewPresenterImpl(api, client, gson);
    }

}
