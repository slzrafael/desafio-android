package test.com.br.challenge.controller;

import android.content.Context;

/**
 * Created by rafaelfreitas on 11/2/16.
 */
public interface PullListView {

    public void showProgressBar();
    public void hideProgressBar();

    public Context getContext();

}
