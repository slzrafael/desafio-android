package test.com.br.challenge.controller;

import java.util.List;

import test.com.br.challenge.model.RepositoryPullRequest;

/**
 * Created by rafaelfreitas on 11/2/16.
 */
public interface PullListViewPresenter {

    public interface OnRequesByPullRequeststFinished {

        public void onSuccess(List<RepositoryPullRequest> repositories);
        public void onError(RuntimeException e);

    }

    public void getListOfPullRequest(int page, String user, String repo, OnRequesByPullRequeststFinished listener);

    public interface OnRequestByOpenAndClosedPullFinished {
        public void onSuccess(int open, int closed);
        public void onError(RuntimeException e);
    }

    public void getNumberOfOpenAndClosedPulls(String user, String repo, OnRequestByOpenAndClosedPullFinished listener);



}
