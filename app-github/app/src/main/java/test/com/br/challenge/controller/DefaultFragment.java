package test.com.br.challenge.controller;

import android.support.v4.app.Fragment;

import test.com.br.challenge.app.ApplicationComponent;
import test.com.br.challenge.app.GithubApplication;

/**
 * Created by rafael-iteris on 11/7/16.
 */

public class DefaultFragment extends Fragment {

    public ApplicationComponent getAppComponent(){
        GithubApplication app = (GithubApplication) getActivity().getApplication();
        return app.getApp();
    }

}
