package test.com.br.challenge.controller;

import java.util.List;

import test.com.br.challenge.model.Repository;

/**
 * Created by rafaelfreitas on 11/2/16.
 */
public interface RepositoryListViewPresenter {

    public void getList(int page, OnRequestFinished listener);

    public interface OnRequestFinished {

        public void onSuccess(List<Repository> repositories);
        public void onError(RuntimeException e);

    }

}
