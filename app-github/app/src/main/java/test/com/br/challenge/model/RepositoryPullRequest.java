package test.com.br.challenge.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rafael-iteris on 10/31/16.
 */
public class RepositoryPullRequest {

    @SerializedName("title")
    private String title;

    @SerializedName("body")
    private String description;

    @SerializedName("user")
    private Owner owner;

    @SerializedName("html_url")
    private String url;

    public RepositoryPullRequest() {}

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
