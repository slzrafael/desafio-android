package test.com.br.challenge.controller;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import test.com.br.challenge.R;
import test.com.br.challenge.adapter.RepositoryPullRequestAdapter;
import test.com.br.challenge.commom.ProgressFeedbackManager;
import test.com.br.challenge.model.RepositoryPullRequest;

/**
 * Created by rafaelfreitas on 11/2/16.
 */
public class PullListViewFragment extends DefaultFragment implements PullListView, PullListViewPresenter.OnRequesByPullRequeststFinished, PullListViewPresenter.OnRequestByOpenAndClosedPullFinished {

    @BindView(R.id.list_pull)
    protected RecyclerView recycler;

    @BindView(R.id.pull_refresh)
    protected SwipeRefreshLayout swype;

    @BindView(R.id.toolbar)
    protected Toolbar toolbar;

    @BindView(R.id.lbl_open_closed_pulls)
    protected TextView openAndClosedPulls;

    @BindView(R.id.fab_up)
    protected FloatingActionButton fab;

    private String owner;
    private String repository;

    private RepositoryPullRequestAdapter adapter;

    @Inject
    protected ProgressFeedbackManager feedback;

    @Inject
    protected PullListViewInteractor interactor;

    @Inject
    protected PullListViewPresenter presenter;

    @Inject
    protected Picasso picasso;

    private int page = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.repository_pulls_fragment, container, false);
        ButterKnife.bind(this, v);

        getAppComponent().inject(this);
        getViewComponent().inject(this);

        LinearLayoutManager layout = new LinearLayoutManager(getActivity());
        DividerItemDecoration divider = new DividerItemDecoration(getActivity(), layout.getOrientation());

        recycler.setLayoutManager(layout);
        recycler.setHasFixedSize(true);
        recycler.addItemDecoration(divider);

        repository = getActivity().getIntent().getExtras().getString("repository");
        owner = getActivity().getIntent().getExtras().getString("user");
        toolbar.setTitleTextColor(getContext().getColor(android.R.color.white));

        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(repository);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setHasOptionsMenu(true);

        return v;
    }

    private PullListComponent getViewComponent() {
        return DaggerPullListComponent.builder()
                .applicationComponent(getAppComponent())
                .pullListModule(new PullListModule(getActivity()))
                .build();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        swype.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                interactor.onPull(++page, owner, repository, PullListViewFragment.this);
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                interactor.onClickToBackToTop(recycler);
            }
        });

        recycler.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                interactor.onScrollRecyclerView(fab, recyclerView);
            }

        });

        showProgressBar();

        presenter.getListOfPullRequest(++page, owner, repository, this);
        presenter.getNumberOfOpenAndClosedPulls(owner, repository, this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home :
                getActivity().onBackPressed();

                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showProgressBar() {
        feedback.show();
    }
    @Override
    public void hideProgressBar() {
        feedback.dismiss();
    }

    @Override
    public void onSuccess(List<RepositoryPullRequest> pulls) {
        hideProgressBar();

        if(adapter == null){
            adapter = new RepositoryPullRequestAdapter(pulls, getContext(), interactor, picasso);
            recycler.setAdapter(adapter);
        } else {
            swype.setRefreshing(false);

            final int lastPositionItem = adapter.getItemCount();

            Snackbar.make(getView(), "Foram carregados novos itens.", Snackbar.LENGTH_LONG).setAction("VER", new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    interactor.onClickToGoForNewItens(recycler, lastPositionItem);
                }

            }).show();

            adapter.getPulls().addAll(pulls);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onSuccess(final int open, final int closed) {
        hideProgressBar();

        openAndClosedPulls.setText("open " + open + " / closed " + closed);
        openAndClosedPulls.setVisibility(View.VISIBLE);
    }

    @Override
    public void onError(RuntimeException e) {
        hideProgressBar();

        new AlertDialog.Builder(getContext())
                .setTitle("Atencão")
                .setMessage("Ocoreu um erro")
                .setNeutralButton("OK", null).show();
    }

}
