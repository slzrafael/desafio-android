package test.com.br.challenge.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import test.com.br.challenge.R;
import test.com.br.challenge.controller.RepositoryListViewInteractor;
import test.com.br.challenge.model.Repository;

/**
 * Created by rafael-iteris on 10/31/16.
 */
public class RepositoryAdapter extends RecyclerView.Adapter {

    private List<Repository> repositories;
    private RepositoryListViewInteractor interactor;
    private Picasso picasso;

    private Context context;

    public RepositoryAdapter(@NonNull  Context context, @NonNull  List<Repository> repositories, RepositoryListViewInteractor interactor, Picasso picasso) {
        this.repositories = repositories;
        this.context = context;
        this.interactor = interactor;
        this.picasso = picasso;
    }

    public List<Repository> getRepositories() {
        return repositories;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflated = LayoutInflater.from(context).inflate(R.layout.row_repository, parent, false);
        return new RepositoryViewHolder(inflated);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final Repository repository = repositories.get(position);

        final RepositoryViewHolder h = (RepositoryViewHolder) holder;
        h.title.setText(repository.getName());
        h.sub.setText(repository.getDescription());

        h.fork.setText(repository.getFork().toString());
        h.star.setText(repository.getStar().toString());

        h.owner.setText(repository.getOwner().getUsername().toLowerCase());
        h.username.setText(repository.getOwner().getUsername());

        picasso.load(repository.getOwner().getAvatarURL())
                .placeholder(R.drawable.placeholder)
                .resize(150, 150).into(h.avatar);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                interactor.onClick(RepositoryAdapter.this, v, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return repositories == null ? 0 : repositories.size();
    }

    public static final class RepositoryViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.lbl_repository_title)
        TextView title;
        @BindView(R.id.lbl_repository_description)
        TextView sub;

        @BindView(R.id.lbl_repository_fork)
        TextView fork;
        @BindView(R.id.lbl_repository_star)
        TextView star;

        @BindView(R.id.img_repository_owner)
        ImageView avatar;
        @BindView(R.id.lbl_repository_name)
        TextView username;
        @BindView(R.id.lbl_repository_owner)
        TextView owner;

        public RepositoryViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

    }

}
