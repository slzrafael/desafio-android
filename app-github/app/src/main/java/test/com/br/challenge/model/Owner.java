package test.com.br.challenge.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rafaelfreitas on 11/2/16.
 */
public class Owner {

    @SerializedName("login")
    private String username;

    private transient String name;

    @SerializedName("avatar_url")
    private String avatarURL;

    public Owner() {
    }

    public Owner(String username, String name, String avatarURL) {
        this.username = username;
        this.name = name;
        this.avatarURL = avatarURL;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatarURL() {
        return avatarURL;
    }

    public void setAvatarURL(String avatarURL) {
        this.avatarURL = avatarURL;
    }
}
