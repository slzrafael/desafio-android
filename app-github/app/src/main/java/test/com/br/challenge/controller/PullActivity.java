package test.com.br.challenge.controller;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import test.com.br.challenge.R;

/**
 * Created by rafaelfreitas on 11/2/16.
 */
public class PullActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.pull_layout);
    }
}
