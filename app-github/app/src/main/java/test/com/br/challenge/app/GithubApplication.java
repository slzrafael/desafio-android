package test.com.br.challenge.app;

import android.app.Application;

import java.io.IOException;

/**
 * Created by rafaelfreitas on 11/6/16.
 */
public class GithubApplication extends Application {

    private ApplicationComponent app;

    @Override
    public void onCreate() {
        app = DaggerApplicationComponent.builder().applicationModule(new ApplicationModule(this)).build();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();

        try {
            app.provideHttpClient().cache().flush();
            app.provideHttpClient().cache().close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        app = null;
    }

    public ApplicationComponent getApp() {
        return app;
    }

}
