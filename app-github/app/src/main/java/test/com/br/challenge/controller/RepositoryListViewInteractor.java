package test.com.br.challenge.controller;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by rafaelfreitas on 11/2/16.
 */
public interface RepositoryListViewInteractor {

    public void onClickToBackToTop(RecyclerView recyclerView);
    public void onClickToGoForNewItens(RecyclerView recyclerView, int lastPosition);
    public void onScrollRecyclerView(FloatingActionButton fab, RecyclerView recyclerView);

    public void onPull(int page, RepositoryListViewPresenter.OnRequestFinished listener);
    public void onClick(RecyclerView.Adapter<?> adapter, View view, int index);

}
