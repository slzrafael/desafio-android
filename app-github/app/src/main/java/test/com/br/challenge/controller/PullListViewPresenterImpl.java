package test.com.br.challenge.controller;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

import javax.inject.Inject;

import test.com.br.challenge.model.RepositoryPullRequest;
import test.com.br.challenge.net.GithubAPI;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rafaelfreitas on 11/2/16.
 */
public class PullListViewPresenterImpl implements PullListViewPresenter {

    private Type type = new TypeToken<List<RepositoryPullRequest>>() {}.getType();

    private GithubAPI client;
    private OkHttpClient http;
    private Gson gson;

    @Inject
    public PullListViewPresenterImpl(GithubAPI api, OkHttpClient http, Gson gson) {
        this.client = api;
        this.http = http;
        this.gson = gson;
    }

    @Override
    public void getListOfPullRequest(int page, String user, String repo, final OnRequesByPullRequeststFinished listener) {
        client.getPullRequest(user, repo, page).enqueue(new Callback<JsonArray>() {

            @Override
            public void onResponse(Call<JsonArray> call, Response<JsonArray> response) {
                JsonArray pulls = response.body();
                List<RepositoryPullRequest> result = gson.fromJson(pulls, type);

                listener.onSuccess(result);
            }

            @Override
            public void onFailure(Call<JsonArray> call, Throwable t) {
                t.printStackTrace();
            }

        });

    }

    /* this info is not available at official api of github */

    @Override
    public void getNumberOfOpenAndClosedPulls(String user, String repo, final OnRequestByOpenAndClosedPullFinished listener) {

        String URL = "https://github.com/{user}/{repository}/pulls"
                .replace("{user}", user)
                .replace("{repository}", repo);

        Request request = new Request.Builder().url(URL).get().build();

        http.newCall(request).enqueue(new okhttp3.Callback() {

            @Override
            public void onResponse(okhttp3.Call call, okhttp3.Response response) throws IOException {
                String body = response.body().string();

                Document root = Jsoup.parse(body);
                final String open = root.body().select("#js-issues-toolbar > div > div.table-list-header-toggle.states.float-left.pl-3 > a:nth-child(1)").text().replace("Open", "").trim();
                final String close = root.body().select("#js-issues-toolbar > div > div.table-list-header-toggle.states.float-left.pl-3 > a:nth-child(2)").text().replace("Closed", "").replace(",", "").trim();

                new Handler(Looper.getMainLooper()) {

                    @Override
                    public void handleMessage(Message msg) {
                        listener.onSuccess(Integer.valueOf(open).intValue(), Integer.valueOf(close).intValue());

                    }

                }.sendEmptyMessage(0);

            }

            @Override
            public void onFailure(okhttp3.Call call, IOException e) {
                //TODO

                e.printStackTrace();
            }

        });

    }

}
