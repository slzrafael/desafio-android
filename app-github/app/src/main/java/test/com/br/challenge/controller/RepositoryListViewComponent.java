package test.com.br.challenge.controller;

import test.com.br.challenge.app.ActivitiyScope;
import test.com.br.challenge.app.ApplicationComponent;
import test.com.br.challenge.commom.ProgressFeedbackManager;
import dagger.Component;

/**
 * Created by rafaelfreitas on 11/7/16.
 */

@ActivitiyScope
@Component(dependencies = ApplicationComponent.class, modules = RepositoryListViewModule.class )
public interface RepositoryListViewComponent {

    public ProgressFeedbackManager provideFeedback();
    public RepositoryListViewInteractor provideViewInteractor();
    public RepositoryListViewPresenter providePresenter();

    public void inject(RepositoryListViewFragment fragment);

}
