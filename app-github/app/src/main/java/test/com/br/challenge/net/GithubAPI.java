package test.com.br.challenge.net;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by rafael-iteris on 10/31/16.
 */
public interface GithubAPI {

    // https://api.github.com/search/repositories?q=language:Java&sort=stars&page=<page>

    @GET("search/repositories?q=language:Java&sort=stars")
    Call<JsonObject> getList(@Query("page") Integer page);

    // https://api.github.com/repos/<criador>/<repositório>/pulls

    @GET("repos/{user}/{repository}/pulls")
    Call<JsonArray> getPullRequest(@Path("user") String user, @Path("repository") String repository, @Query("page") int page);

    @GET("/users/{user}")
    Call<JsonObject> getInfoAboutUser(@Path("user") String user);

}
