package test.com.br.challenge.controller;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import javax.inject.Inject;

import test.com.br.challenge.model.Repository;
import test.com.br.challenge.net.GithubAPI;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by rafaelfreitas on 11/2/16.
 */
public class RepositoryListViewPresenterImp implements RepositoryListViewPresenter {

    private Type type = new TypeToken<List<Repository>>() {}.getType();

    protected GithubAPI client;
    protected Gson gson;

    @Inject
    public RepositoryListViewPresenterImp(GithubAPI api, Gson gson) {
        this.client = api;
        this.gson = gson;
    }

    @Override
    public void getList(int page, final OnRequestFinished listener) {
        client.getList(page).enqueue(new Callback<JsonObject>() {

            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                JsonArray itens = response.body().getAsJsonObject().getAsJsonArray("items");
                List<Repository> list = gson.fromJson(itens, type);

                listener.onSuccess(list);
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                listener.onError(new RuntimeException(t));
            }

        });
    }

}
