package test.com.br.challenge.app;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by rafaelfreitas on 11/7/16.
 */

@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface ActivitiyScope {
}
