package test.com.br.challenge.controller;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import javax.inject.Inject;

import test.com.br.challenge.adapter.RepositoryPullRequestAdapter;
import test.com.br.challenge.model.RepositoryPullRequest;

/**
 * Created by rafaelfreitas on 11/2/16.
 */
public class PullListViewInteractorImpl implements PullListViewInteractor {

    private PullListViewPresenter presenter;
    private Context context;

    @Inject
    public PullListViewInteractorImpl(Context context, PullListViewPresenter presenter){
        this.context = context;
        this.presenter = presenter;
    }

    @Override
    public void onClickToBackToTop(RecyclerView recycler) {
        recycler.getLayoutManager().scrollToPosition(0);
    }

    @Override
    public void onClickToGoForNewItens(RecyclerView recycler, int lastPosition) {
        ((LinearLayoutManager) recycler.getLayoutManager()).scrollToPositionWithOffset(lastPosition, 0);
    }

    @Override
    public void onScrollRecyclerView(FloatingActionButton fab, RecyclerView recycler) {
        LinearLayoutManager linear = (LinearLayoutManager) recycler.getLayoutManager();

        if(linear.findFirstCompletelyVisibleItemPosition() == 0){
            fab.setVisibility(View.INVISIBLE);
        } else {
            fab.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onPull(int page, String owner, String repository, PullListViewPresenter.OnRequesByPullRequeststFinished listener) {
        presenter.getListOfPullRequest(++page, owner, repository, listener);
    }

    @Override
    public void onClick(RecyclerView.Adapter<?> adapter, View view, int index) {
        RepositoryPullRequestAdapter adpt = (RepositoryPullRequestAdapter) adapter;
        RepositoryPullRequest selected = adpt.getPulls().get(index);

        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(selected.getUrl()));

        context.startActivity(intent);
    }

}
