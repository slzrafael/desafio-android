package test.com.br.challenge.controller;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import javax.inject.Inject;

import test.com.br.challenge.adapter.RepositoryAdapter;
import test.com.br.challenge.model.Repository;

/**
 * Created by rafaelfreitas on 11/2/16.
 */
public class RepositoryListViewInteractorImpl implements RepositoryListViewInteractor {

    private Context context;
    private RepositoryListViewPresenter presenter;

    @Inject
    public RepositoryListViewInteractorImpl(Context context, RepositoryListViewPresenter presenter) {
        this.context = context;
        this.presenter = presenter;
    }

    @Override
    public void onClickToBackToTop(RecyclerView recycler) {
        recycler.getLayoutManager().scrollToPosition(0);
    }

    @Override
    public void onClickToGoForNewItens(RecyclerView recycler, int lastPosition) {
        ((LinearLayoutManager) recycler.getLayoutManager()).scrollToPositionWithOffset(lastPosition, 0);
    }

    @Override
    public void onScrollRecyclerView(FloatingActionButton fab, RecyclerView recyclerView) {
        LinearLayoutManager linear = (LinearLayoutManager) recyclerView.getLayoutManager();

        if(linear.findFirstCompletelyVisibleItemPosition() == 0){
            fab.setVisibility(View.INVISIBLE);
        } else {
            fab.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onPull(int page, RepositoryListViewPresenter.OnRequestFinished listener) {
        presenter.getList(page, listener);
    }

    @Override
    public void onClick(RecyclerView.Adapter<?> adapter, View view, int index) {
        Repository repository = ((RepositoryAdapter) adapter).getRepositories().get(index);

        Bundle bundle = new Bundle();
        bundle.putString("user", repository.getOwner().getUsername());
        bundle.putString("repository", repository.getName());

        Intent intent = new Intent(context, PullActivity.class);
        intent.putExtras(bundle);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        context.startActivity(intent);
    }

}
