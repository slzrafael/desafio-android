package test.com.br.challenge.commom;

/**
 * Created by rafaelfreitas on 11/2/16.
 */
public interface ProgressFeedbackManager {

    public void show();
    public void dismiss();

}
