package test.com.br.challenge.app;

import android.content.Context;


import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import test.com.br.challenge.controller.DefaultFragment;
import test.com.br.challenge.net.GithubAPI;
import test.com.br.challenge.net.HTTPModule;
import dagger.Component;
import okhttp3.OkHttpClient;

/**
 * Created by rafaelfreitas on 11/6/16.
 */

@Singleton
@Component(modules = {
        ApplicationModule.class,
        HTTPModule.class
})
public interface ApplicationComponent {

    public Context provideContext();
    public Gson provideGson();

    public GithubAPI provideAPI();
    public OkHttpClient provideHttpClient();
    public Picasso providePicasso();

    public void inject(DefaultFragment fragment);

}
