package test.com.br.challenge.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import test.com.br.challenge.R;
import test.com.br.challenge.controller.PullListViewInteractor;
import test.com.br.challenge.model.RepositoryPullRequest;

/**
 * Created by rafael-iteris on 10/31/16.
 */
public class RepositoryPullRequestAdapter extends RecyclerView.Adapter {

    private Picasso picasso;
    private PullListViewInteractor listener;
    private List<RepositoryPullRequest> pulls;
    private Context context;

    public RepositoryPullRequestAdapter(List<RepositoryPullRequest> pulls, Context context, PullListViewInteractor listener, Picasso picasso) {
        this.pulls = pulls;
        this.context = context;
        this.listener = listener;
        this.picasso = picasso;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflated = LayoutInflater.from(context).inflate(R.layout.row_repository_pull_request, parent, false);
        return new PullHolder(inflated);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        RepositoryPullRequest pull = pulls.get(position);

        PullHolder h = (PullHolder) holder;
        h.title.setText(pull.getTitle());
        h.sub.setText(pull.getDescription());
        h.username.setText(pull.getOwner().getUsername());
        h.name.setText(pull.getOwner().getUsername());

        picasso.load(pull.getOwner().getAvatarURL())
                .placeholder(R.drawable.placeholder)
                .resize(150, 150).into(h.avatar);

        h.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onClick(RepositoryPullRequestAdapter.this, view, position);
            }
        });
    }

    public List<RepositoryPullRequest> getPulls() {
        return pulls;
    }

    @Override
    public int getItemCount() {
        return pulls == null ? 0 : pulls.size();
    }

    public static class PullHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.lbl_pull_title)
        protected TextView title;
        @BindView(R.id.lbl_pull_description)
        protected TextView sub;
        @BindView(R.id.lbl_pull_owner)
        protected TextView username;
        @BindView(R.id.lbl_pull_owner_name)
        protected TextView name;

        @BindView(R.id.img_pull_owner)
        protected ImageView avatar;

        public PullHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

    }


}
