package test.com.br.challenge.controller;

/**
 * Created by rafaelfreitas on 11/2/16.
 */
public interface RepositoryListView {

    public void showProgressBar();
    public void hideProgressBar();

}
