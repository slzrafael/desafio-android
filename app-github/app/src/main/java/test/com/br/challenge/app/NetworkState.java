package test.com.br.challenge.app;

/**
 * Created by rafael-iteris on 11/7/16.
 */

public class NetworkState {

    private boolean isConnected;

    public NetworkState(boolean isConnected) {
        this.isConnected = isConnected;
    }

    public boolean isConnected() {
        return isConnected;
    }
}
