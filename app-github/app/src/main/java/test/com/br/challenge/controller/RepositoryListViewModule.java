package test.com.br.challenge.controller;

import android.app.Activity;
import android.content.Context;

import com.google.gson.Gson;

import test.com.br.challenge.app.ActivitiyScope;
import test.com.br.challenge.commom.ProgressFeedbackManager;
import test.com.br.challenge.commom.ViewProgressFeedbackManagerImpl;
import test.com.br.challenge.net.GithubAPI;
import dagger.Module;
import dagger.Provides;

/**
 * Created by rafaelfreitas on 11/7/16.
 */

@Module
public class RepositoryListViewModule {

    private Activity activity;

    public RepositoryListViewModule(Activity activity) {
        this.activity = activity;
    }

    @Provides @ActivitiyScope
    public RepositoryListViewInteractor provideInteractor(Context context, RepositoryListViewPresenter presenter){
        return new RepositoryListViewInteractorImpl(context, presenter);
    }

    @Provides @ActivitiyScope
    public RepositoryListViewPresenter providePresenter(GithubAPI api, Gson gson){
        return new RepositoryListViewPresenterImp(api, gson);
    }

    @Provides @ActivitiyScope
    public ProgressFeedbackManager provideFeedback(){
        return new ViewProgressFeedbackManagerImpl(activity);
    }


}
