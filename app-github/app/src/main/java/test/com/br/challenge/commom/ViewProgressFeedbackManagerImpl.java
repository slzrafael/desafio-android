package test.com.br.challenge.commom;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by rafaelfreitas on 11/2/16.
 */
public class ViewProgressFeedbackManagerImpl implements ProgressFeedbackManager {

    private Activity activity;
    private ProgressDialog dialog;

    public ViewProgressFeedbackManagerImpl(Context context) {
        if(!(context instanceof Activity))
            throw new IllegalArgumentException("O contexto deve ser de uma activity");

        this.activity = (Activity) context;
    }

    @Override
    public synchronized void show() {
        dialog = new ProgressDialog(activity);
        dialog.setTitle("Atenção");
        dialog.setMessage("Aguarde um instance...");

        if(!activity.isFinishing())
            dialog.show();
    }

    @Override
    public synchronized void dismiss() {
        if(dialog != null && dialog.isShowing())
            dialog.dismiss();
    }

}
