package test.com.br.challenge.controller;

import test.com.br.challenge.app.ActivitiyScope;
import test.com.br.challenge.app.ApplicationComponent;
import test.com.br.challenge.commom.ProgressFeedbackManager;
import dagger.Component;

/**
 * Created by rafael-iteris on 11/7/16.
 */

@ActivitiyScope
@Component(dependencies = ApplicationComponent.class, modules = PullListModule.class)
public interface PullListComponent {

    public void inject(PullListViewFragment fragment);

    public ProgressFeedbackManager provideFeedback();
    public PullListViewInteractor provideInteractor();
    public PullListViewPresenter providePresenter();

}
