package test.com.br.challenge.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by rafael-iteris on 10/31/16.
 */
public class Repository {

    @SerializedName("owner")
    private Owner owner;

    @SerializedName("name")
    private String name;

    @SerializedName("description")
    private String description;

    @SerializedName("forks_count")
    private Integer fork;

    @SerializedName("stargazers_count")
    private Integer star;

    public Repository() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getFork() {
        return fork;
    }

    public void setFork(Integer fork) {
        this.fork = fork;
    }

    public Integer getStar() {
        return star;
    }

    public void setStar(Integer star) {
        this.star = star;
    }

    public Owner getOwner() {
        return owner;
    }

    public void setOwner(Owner owner) {
        this.owner = owner;
    }

}
